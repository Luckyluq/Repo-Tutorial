## Checklist

1.  Membuat Halaman Chat Box
    1. [ x ] Tambahkan _lab_6.html_ pada folder templates (Membuat template tampilan aplikasi lab 6, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/templates/lab_6/lab_6.html)
    2. [ x ] Tambahkan _lab_6.css_ pada folder _./static/css_ (Mengatur tampilan aplikasi lab 6, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/css/lab_6.css)
    3. [ x ] Tambahkan file _lab_6.js_ pada folder _lab_6/static/js_ ( Untuk melakukan Asynchronous Web, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js)
    4. [ x ] Lengkapi potongan kode pada _lab_6.js_ agar dapat berjalan ( Melengkapi chatbox, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js/lab_6.js)

2. Mengimplementasikan Calculator
    1. [ x ] Tambahkan potongan kode ke dalam file _lab_6.html_ pada folder templates (Menambahkan peletakan kalkulator, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/templates/lab_6/lab_6.html)
    2. [ x ] Tambahkan potongan kode ke dalam file _lab_6.css_ pada folder _./static/css_ (Memperelegan desain kalkulator, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/css/lab_6.css)
    3. [ x ] Tambahkan potongan kode ke dalam file _lab_6.js_ pada folder _lab_6/static/js_ (Menjalankan kalkulator, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js/lab_6.js)
    4. [ x ] Implementasi fungsi `AC`. (Sudah ditambahkan, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/templates/lab_6/layout/base.html)

3.  Mengimplementasikan select2
    1. [ x ] Load theme default sesuai selectedTheme (Set default theme, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js/lab_6.js)
    2. [ x ] Populate data themes dari local storage ke select2 (Sudah dibuat, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js/lab_6.js)
    3. [ x ] Local storage berisi themes dan selectedTheme(Sudah diinisiasi, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js/lab_6.js)
    4. [ x ] Warna berubah ketika theme dipilih(Sudah ditambahkan tombol apply dan bisa diganti, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js/lab_6.js)

4.  Pastikan kalian memiliki _Code Coverage_ yang baik
    1. [ x ]  Jika kalian belum melakukan konfigurasi untuk menampilkan _Code Coverage_ di Gitlab maka lihat langkah `Show Code Coverage in Gitlab` di [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md).
    2. [ x ] Pastikan _Code Coverage_ kalian 100%. (https://gitlab.com/Luckyluq/Repo-Tutorial/-/jobs/40600656)

###  Challenge Checklist
1. Latihan Qunit
    1. [ x ] Implementasi dari latihan Qunit (Menambahi script di lab_6.html https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/templates/lab_6/lab_6.html dan melengkapi test.js https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js/test.js)
2. Cukup kerjakan salah satu nya saja:
    1. Implementasikan tombol enter pada chat box yang sudah tersedia
        1. [ ] Buatlah sebuah _Unit Test_ menggunakan Qunit
        2. [ ] Bulah fungsi yang membuat _Unit Test_ Tersebut _passed_ 
    2. Implementasikan fungsi `sin`, `log`, dan `tan`. (HTML sudah tersedia di dalam potongan kode)
    (Menambahkan sin log tan di lab_6.html https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/templates/lab_6/lab_6.html, dan lab_6.js https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js/lab_6.js)
        1. [ x ] Buatlah sebuah _Unit Test_ menggunakan Qunit
        2. [ x ] Bulah fungsi yang membuat _Unit Test_ Tersebut _passed_
        (Unit test sudah passed, https://gitlab.com/Luckyluq/Repo-Tutorial/blob/master/lab_6/static/js/test.js)