from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    name_attrs = {
        'class': 'form-control',
        'placeholder': 'BoBoiBoy Thunderstorm',
    }

    email_attrs = {
        'class': 'form-control',
        'placeholder': 'exampleboi@boiboi.com',
    }
   
    message_attrs = {
        'class': 'form-control',
        'placeholder': 'Boi send me a mesej.',
    }

    name = forms.CharField(label='Nama', required=False, max_length=27, empty_value='Anonymous', widget=forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(required=False, widget=forms.EmailInput(attrs=email_attrs))
    message = forms.CharField(widget=forms.Textarea(attrs=message_attrs), required=True)
